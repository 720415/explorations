// Exploration of RGB and HSB(V) colors.

use std::fmt;
use std::ops::{Add, Sub};

use lazy_static::lazy_static;
use regex::Regex;

// A basic color respresentation, primarily in RGB form.
#[derive(PartialEq, Debug, Copy, Clone)]
struct Color {
	red: f64,
	green: f64,
	blue: f64,
}

impl fmt::Display for Color {
	// Present this Color in readable string format.
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let fix = |val| val * 255.0;
		write!(f, "Color:{{red: {}, green: {}, blue: {}}}", fix(self.red), fix(self.green), fix(self.blue))
	}
}

impl Add for Color {
	type Output = Self;
	
	// Create a new Color that is the sum of the two Colors.
	fn add(self, rhs: Self) -> Self::Output {
		let limit = |val: f64| (val * 255.0).min(255.0).max(0.0).round() as u8;
		Color::from_rgb(
			limit(self.red + rhs.red),
			limit(self.green + rhs.green),
			limit(self.blue + rhs.blue),
		)
	}
}

impl Sub for Color {
	type Output = Self;
	
	// Create a new Color that is the sum of the two Colors.
	fn sub(self, rhs: Self) -> Self::Output {
		let limit = |val: f64| (val * 255.0).min(255.0).max(0.0).round() as u8;
		Color::from_rgb(
			limit(self.red - rhs.red),
			limit(self.green - rhs.green),
			limit(self.blue - rhs.blue),
		)
	}
}

impl Color {
	fn new() -> Self {
		Self {
			red: 0.0,
			green: 0.0,
			blue: 0.0,
		}
	}
	
	fn from_rgb(red: u8, green: u8, blue: u8) -> Self {
		let fraction = |x: u8| -> f64 { x as f64 / 255.0 };
		Self {
			red: fraction(red),
			green: fraction(green),
			blue: fraction(blue),
		}
	}
	
	fn from_int(val: u32) -> Self {
		Self::from_rgb(
			((val & 0xff0000) >> 16) as u8,
			((val & 0xff00) >> 8) as u8,
			(val & 0xff) as u8,
		)
	}
	
	fn from_hex(hex_val: &str) -> Self {
		lazy_static! {
			static ref RE: Regex = Regex::new(r"^0x([0-9A-Fa-f]{6})$").unwrap();
		}
		if RE.is_match(hex_val) {
			let matches = RE.captures(hex_val).unwrap();
			let value = u32::from_str_radix(&matches[1], 16).unwrap();
			Self::from_int(value)
		}
		else {
			Self::new()
		}
	}
	
	// The value of this color (unrounded).
	fn value_raw(&self) -> f64 {
		self.red.max(self.green).max(self.blue)
	}
	
	// The value of this color (rounded).
	fn value(&self) -> f64 {
		format!("{:.3}", self.value_raw()).parse().unwrap()
	}
	
	// The chroma of this color (unrounded).
	fn chroma_raw(&self) -> f64 {
		self.value_raw() - self.red.min(self.green).min(self.blue)
	}
	
	// The chroma of this color (rounded).
	fn chroma(&self) -> f64 {
		format!("{:.3}", self.chroma_raw()).parse().unwrap()
	}
	
	// The saturation of this color (unrounded).
	fn saturation_raw(&self) -> f64 {
		let val = self.value();
		if val != 0.0 {
			self.chroma() / val
		}
		else {
			0.0
		}
	}
	
	// The saturation of this color (rounded).
	fn saturation(&self) -> f64 {
		format!("{:.3}", self.saturation_raw()).parse().unwrap()
	}
	
	// The hue of this color (unrounded).
	fn hue_raw(&self) -> f64 {
		let val = self.value();
		let chroma = self.chroma();
		let mut hue = 0.0;
		let eq = |left: f64, right: f64| -> bool { (left - right) <= f64::EPSILON };
		
		if chroma == 0.0 {
		}
		else if eq(val, self.red) {
			hue = ((self.green - self.blue) / chroma) % 6.0;
		}
		else if eq(val, self.green) {
			hue = ((self.blue - self.red) / chroma) + 2.0;
		}
		else if eq(val, self.blue) {
			hue = ((self.red - self.green) / chroma) + 4.0;
		}
		hue * 60.0
	}
	
	// The hue of this color (rounded).
	fn hue(&self) -> f64 {
		format!("{:.3}", self.hue_raw()).parse().unwrap()
	}
	
	// This color in HSV form (unrounded).
	fn hsv_raw(&self) -> (f64, f64, f64) {
		(self.hue_raw(), self.saturation_raw(), self.value_raw())
	}
	
	// This color in HSV form (rounded).
	fn hsv(&self) -> (f64, f64, f64) {
		(self.hue(), self.saturation(), self.value())
	}
	
	// Get a hex code of this color for program use.
	fn hex_code(&self) -> String {
		let fix = |val: f64| -> u32 { (val * 255.0).round() as u32 };
		format!("0x{:02x}{:02x}{:02x}", fix(self.red), fix(self.green), fix(self.blue))
	}
	
	// Get a web code of this color for program use.
	fn web_code(&self) -> String {
		let fix = |val: f64| -> u32 { (val * 255.0).round() as u32 };
		format!("#{:02x}{:02x}{:02x}", fix(self.red), fix(self.green), fix(self.blue))
	}
}

// Convert a HSV color to RGB by formulas.
fn hsv_rgb_formulas(mut hue: f64, mut sat: f64, mut value: f64) -> [u8; 3] {
	hue /= 60.0;
	if sat > 1.0 {
		sat /= 100.0;
	}
	if value > 1.0 {
		value /= 100.0;
	}
	
	let k_val = |n| (hue + n) % 6.0;
	let get_part = |n| value - (sat * value * 1.0_f64.min(4.0 - k_val(n)).min(k_val(n)).max(0.0));
	[5.0, 3.0, 1.0].map(get_part).map(|val| (val * 255.0).round() as u8)
}

// Convert a HSV color to RGB by table.
fn hsv_rgb_table(mut hue: f64, mut sat: f64, mut value: f64) -> [u8; 3] {
	hue /= 60.0;
	if sat > 1.0 {
		sat /= 100.0;
	}
	if value > 1.0 {
		value /= 100.0;
	}
	
	let mut rgb = [0.0, 0.0, 0.0];
	
	let chroma = sat * value;
	let mid = chroma * (1.0 - ((hue % 2.0) - 1.0).abs());
	if mid != 0.0 {
		if (2.0..3.0).contains(&hue) || (5.0..6.0).contains(&hue) {
			rgb[2] = mid;
		}
		else if (0.0..1.0).contains(&hue) || (3.0..4.0).contains(&hue) {
			rgb[1] = mid;
		}
		else {
			rgb[0] = mid;
		}
	}
	
	if (3.0..=5.0).contains(&hue) {
		rgb[2] = chroma;
	}
	else if (1.0..=3.0).contains(&hue) {
		rgb[1] = chroma;
	}
	else {
		rgb[0] = chroma;
	}
	
	rgb.map(|val| val + value - chroma).map(|val| (val * 255.0).round() as u8)
}

fn main() {
	let test_case: u32 = 0x362698;
	let test_red = ((test_case & 0xff0000) >> 16) as u8;
	let test_green = ((test_case & 0xff00) >> 8) as u8;
	let test_blue = (test_case & 0xff) as u8;
	println!("({}, {}, {})\n", test_red, test_green, test_blue);
	
	let my_color = Color::from_rgb(test_red, test_green, test_blue);
	let (hsv_red, hsv_green, hsv_blue) = my_color.hsv_raw();
	println!("({}, {}, {})", hsv_red, hsv_green, hsv_blue);
	println!("{:?}", my_color.hsv());
	println!("{:?}", hsv_rgb_table(hsv_red, hsv_green, hsv_blue));
	println!("{:?}", hsv_rgb_formulas(hsv_red, hsv_green, hsv_blue));
	print!("{}", my_color.hex_code());
	if my_color.hex_code() == format!("0x{:x}", test_case) {
		println!(" same");
	}
	else {
		println!(" not same");
	}
	
	let my_color2 = Color::from_hex(format!("0x{:x}", test_case).as_str());
	println!("{:?}", my_color2.hsv());
	println!("{}", my_color2);
	println!("{:?}", Color::from_int(test_case).hsv());
	println!("{:?}", (my_color2 + Color::from_int(0x010101)).hsv());
	
	let my_color3 = my_color2 - Color::from_int(0x020202);
	println!("{:?}", my_color3.hsv());
	println!("{}", my_color3.web_code());
}

// vim: set ts=4 sw=0 sts=-1 noex ai sta:
