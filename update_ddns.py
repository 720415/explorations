#!/usr/bin/python3

'''A script to update a Cloudflare DDNS entry.'''

from argparse import ArgumentParser
from json import dumps
from sys import argv, exit as sys_exit, stderr
from typing import Sequence

from requests import RequestException, get

def public_ip() -> str:
	'''Get the public IP (v4) of this computer.'''
	
	try:
		response = get('https://api.ipify.org', params={'format': 'json'})
		response.raise_for_status()
		return response.json()['ip']
	
	except (RequestException, KeyError) as exc:
		print(exc, file=stderr)
		sys_exit(1)

def make_parser() -> ArgumentParser:
	'''Make and return a suitable cmd line argument parser.'''
	
	from argparse import FileType, RawTextHelpFormatter as RTHF
	
	prefix = 'ddns'
	example = {'token': '<API_TOKEN>', 'zone': 'example.com', 'prefix': 'home'}
	
	epilog = 'Configuration is expected to have entries for an API Token and zone name.'
	epilog += '\nThe configuration may also provide a different record name prefix.'
	epilog += f"\n   Default prefix: '{prefix}'"
	epilog += f"\n\nExample:\n{dumps(example, indent=4)}"
	
	parser = ArgumentParser(
		description=globals()['__doc__'], epilog=epilog, formatter_class=RTHF, allow_abbrev=False
	)
	
	script_version = (1, 0)
	version_format = f"%(prog)s v{script_version[0]}.{script_version[1]}"
	parser.add_argument('-v', '--version', action='version', version=version_format)
	
	#This token must have #zone:read and #dns_records:edit permissions.
	file_help = 'path to json configuration (see note)'
	parser.add_argument(
		'config', type=FileType('r', encoding='utf-8'), help=file_help, metavar='CONFIG_FILE'
	)
	
	parser.prefix = prefix #type:ignore
	return parser

def main(arguments: Sequence[str], /) -> int:
	'''Main function; update the DNS entry.'''
	
	#pylint: disable=too-many-locals(R0914)
	
	from json import loads
	
	from requests import patch
	
	parser = make_parser()
	# File should not be bigger than 128 bytes.
	config = loads(parser.parse_args(arguments).config.read(128))
	
	api_base = 'https://api.cloudflare.com/client/v4/zones'
	headers = {'Content-Type': 'application/json', 'Authorization': f"Bearer {config['token']}"}
	
	zones = get(api_base, headers=headers, params={'name': config['zone']})
	try:
		zones.raise_for_status()
		zone_id = zones.json()['result'][0]['id'] # Desired data should be first and only entry.
	
	except (RequestException, IndexError, KeyError) as exc:
		if isinstance(exc, (RequestException,)):
			print(exc, file=stderr)
		print(f"\tInvalid zone name ({config['zone']}). Does it exist?", file=stderr)
		return 1
	
	records_path = f"{api_base}/{zone_id}/dns_records"
	# This DNS entry must be created manually before this script can update it.
	prefix = config.get('prefix', parser.prefix) #type:ignore
	record_query = {'type': 'A', 'name': f"{prefix}.{config['zone']}"}
	records = get(records_path, headers=headers, params=record_query)
	try:
		records.raise_for_status()
		record_id = records.json()['result'][0]['id'] # Desired data should be first and only entry.
	
	except (RequestException, IndexError, KeyError) as exc:
		if isinstance(exc, (RequestException,)):
			print(exc, file=stderr)
		print(f"\tInvalid record name ({record_query['name']}). Does it exist?", file=stderr)
		return 1
	
	patch_body = {'content': public_ip(), 'ttl': 1, 'proxied': True}
	response = patch(f"{records_path}/{record_id}", headers=headers, data=dumps(patch_body))
	try:
		response.raise_for_status()
	
	except RequestException as exc:
		print(exc, file=stderr)
		return 1
	
	print(f"\tRecord ({record_query['name']}) updated to ({patch_body['content']}).")
	return 0

if __name__ == '__main__':
	sys_exit(main(argv[1:]))
