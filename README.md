# explorations

A collection of explorations into interesting coding problems.

## Contributing
I will modify/add/remove code from here as I see fit.
PRs are welcome, but may be refused for any reason.
It may also be a while before I review the PR.

## License
Licensed under Apache License 2.0 by Patrick Jenson
