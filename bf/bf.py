#!/usr/bin/python3

'''An implementation of the BrainF**k language interpreter.'''

from argparse import ArgumentParser
from sys import argv, exit as sys_exit
from typing import Sequence

class InfiniteLinearMemory(list):
	'''Unidirectional auto-resizing tape-style memory model.'''
	
	class Address(int):
		'''Restriction of int to N+.'''
		
		def __new__(cls, value: int = 0):
			return super().__new__(cls, max(0, value))
	
	def verify_size(self, index: int, /) -> None:
		'''Ensure that there is enough space for accessed value.'''
		
		if index >= len(self):
			self.extend([0] * (index + 1 - len(self)))
	
	def __setitem__(self, index: Address, value: int, /) -> None: #type:ignore
		assert index >= 0, 'Positive addresses must be used.'
		self.verify_size(index)
		list.__setitem__(self, index, int(value % pow(2, 8))) # Wrap values into 8 unsigned bits.
	
	def __getitem__(self, index: Address, /) -> int: #type:ignore
		assert index >= 0, 'Positive addresses must be used.'
		self.verify_size(index)
		return list.__getitem__(self, index)

def main(arguments: Sequence[str], /) -> int:
	'''Main function.'''
	
	#pylint: disable=too-many-locals(R0914)
	
	from argparse import FileType, RawTextHelpFormatter as RTHF
	from readchar import readchar
	
	# See: https://en.wikipedia.org/wiki/Brainfuck
	parser = ArgumentParser(description=globals()['__doc__'], formatter_class=RTHF, allow_abbrev=False)
	input_help = 'path to file containing BF program'
	parser.add_argument('bf_program', type=FileType('r'), help=input_help, metavar='FILE')
	input_file = parser.parse_args(arguments).bf_program
	
	my_tape = InfiniteLinearMemory()
	data_pointer = InfiniteLinearMemory.Address()
	loop_points = []
	
	def increment_pointer() -> None:
		nonlocal data_pointer
		data_pointer += 1 #type:ignore
	
	def decrement_pointer() -> None:
		nonlocal data_pointer
		data_pointer -= 1 #type:ignore
	
	def increment_cell() -> None:
		my_tape[data_pointer] += 1
	
	def decrement_cell() -> None:
		my_tape[data_pointer] -= 1
	
	def print_cell() -> None:
		print(chr(my_tape[data_pointer]), end='')
	
	def get_data() -> None:
		my_tape[data_pointer] = ord(readchar())
	
	def start_loop() -> None:
		if my_tape[data_pointer] != 0:
			loop_points.append(input_file.tell())
		else:
			if loop_points[-1] == input_file.tell():
				loop_points.pop()
			while input_file.read(1) != ']':
				pass
	
	def end_loop() -> None:
		if my_tape[data_pointer] == 0:
			loop_points.pop()
		else:
			input_file.seek(loop_points[-1])
	
	def dump_state() -> None:
		print(f"Loops: {loop_points}, DP = {data_pointer},\nCells: {my_tape}")
	
	commands = {
		'>': increment_pointer,
		'<': decrement_pointer,
		'+': increment_cell,
		'-': decrement_cell,
		'.': print_cell,
		',': get_data,
		'[': start_loop,
		']': end_loop,
		'|': dump_state, # This is a debugging command I added to BF.
	}
	
	while cmd := input_file.read(1):
		commands.get(cmd, lambda: None)()
	
	print('')
	return 0

if __name__ == "__main__":
	sys_exit(main(argv[1:]))

# vim: set ts=4 sw=0 sts=-1 noet ai sta:
