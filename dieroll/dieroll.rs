// A simple program to simulate rolling arbitrary dice.

use std::env;
use std::io::{stdin, stdout, Write};
use std::path::Path;
use std::sync::{
	atomic::{AtomicBool, Ordering::SeqCst},
	Arc,
};

use lazy_static::lazy_static;
use rand::{thread_rng, Rng};
use regex::Regex;
use signal_hook::{consts::signal::SIGINT, flag::register};

type DiceResult = Vec<(String, Option<u8>)>;

fn build_dice_pattern() -> Regex {
	let named_group = |name: &str| format!(r"(?P<{}>[1-9][0-9]*)", name);
	let operator_group = format!(r"(?:(?P<operator>[+\-*/]){})", named_group("shift"));
	let dice_group_pattern = format!(
		r"^{}d(?P<sides>(?:[1-9][0-9]*|%))(?P<advantage>a)?{}?$",
		named_group("number"),
		operator_group
	);
	Regex::new(&dice_group_pattern).unwrap()
}

// Generate ouput for given dice.
fn calculate_dice(dice_list: &Vec<String>) -> DiceResult {
	lazy_static! {
		static ref DIE_UNIT: Regex = build_dice_pattern();
	}
	
	let mut results = DiceResult::new();
	
	for die in dice_list {
		let mut die_info = None;
		if DIE_UNIT.is_match(die) {
			let match_info = DIE_UNIT.captures(die).unwrap();
			die_info = Some((
				match_info["number"].parse::<u8>().unwrap(),
				match_info["sides"].to_owned(),
				match_info.name("advantage"),
				match_info.name("operator"),
				match_info.name("shift"),
			));
		}
		
		let mut result = None;
		if let Some(die_vals) = die_info {
			let mut rolled_dice: Vec<u8> = Vec::new();
			for _ in 0..die_vals.0 {
				if die_vals.1 == "%" {
					rolled_dice.push(thread_rng().gen_range(0..10) * 10);
				}
				else {
					let n: u8 = die_vals.1.parse().unwrap();
					rolled_dice.push(thread_rng().gen_range(1..=n));
				}
			}
			
			let mut tmp: u8;
			if die_vals.2.is_none() {
				tmp = rolled_dice.iter().sum::<u8>();
			}
			else {
				tmp = *rolled_dice.iter().max().unwrap();
			}
			
			if let Some(operator) = die_vals.3 {
				if let Some(shift) = die_vals.4 {
					let offset = shift.as_str().parse::<u8>().unwrap();
					match operator.as_str() {
						"+" => tmp += offset,
						"-" => tmp -= offset,
						"*" => tmp *= offset,
						"/" => tmp /= offset,
						_ => unreachable!(),
					}
				}
			}
			result = Some(tmp);
		}
		results.push((die.clone(), result));
	}
	results
}

// Display the reults of the dice toss.
fn print_results(results: &DiceResult) {
	let longest = results.iter().max_by_key(|item| item.0.len()).unwrap().0.len();
	for (dice, dset) in results {
		let result = match dset {
			Some(val) => val.to_string(),
			None => String::from("Unknown dice"),
		};
		println!("\n    {:>longest$} = {}", dice, result);
	}
	println!();
}

fn main() {
	let args: Vec<String> = env::args().collect();
	
	let running = Arc::new(AtomicBool::new(false));
	register(SIGINT, Arc::clone(&running)).unwrap();
	
	if args.len() > 1 {
		if args[1] == "-h" {
			let exe = Path::new(&args[0]).file_name().unwrap().to_str().unwrap();
			println!("\nusage: {} [-h] [NdS[A][<OP>M] [ DG[ ...]]]\nwhere:", exe);
			println!("\tN is a number of dice to roll");
			println!("\tS is the number of sides on each of those dice");
			println!("\tA is a literal 'a' to indicate rolling with advantage");
			println!("\tOP is an optional modifier for each group; one of {{+-*/}}");
			println!("\tM is the value for the modifier");
			println!("\tDG is another dice group like above");
			println!("\nOmit all dice to enter a rolling shell.");
			println!("A blank line will re-run the previous line.");
			println!("^C will exit the rolling shell.");
		}
		else {
			print_results(&calculate_dice(&args[1..].to_vec()));
		}
	}
	else {
		let mut prev_dice = Vec::new();
		
		while !running.load(SeqCst) {
			print!("\nd> ");
			stdout().flush().unwrap();
			
			let mut input = String::new();
			if stdin().read_line(&mut input).is_ok() {
				let mut dice = Vec::new();
				for part in input.split_whitespace().collect::<Vec<&str>>() {
					dice.push(String::from(part));
				}
				
				if !dice.is_empty() {
					prev_dice = dice.clone();
					print_results(&calculate_dice(&dice));
				}
				else if !prev_dice.is_empty() {
					//~TODO: Put this on the same line as the prompt; like it had been typed.
					println!(":  {}", prev_dice.join(" "));
					print_results(&calculate_dice(&prev_dice));
				}
			}
		}
	}
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
