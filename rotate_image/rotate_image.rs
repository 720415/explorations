// An exploration into simple numeric 'image' manipulation.

use std::env;
use std::fmt;
use std::ops::{Index, IndexMut};

#[derive(Debug)]
struct Image {
	image: Vec<Vec<usize>>,
}

impl fmt::Display for Image {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let mut width = 0;
		for row in self.image.as_slice() {
			width = width.max(row.iter().cloned().fold(0, usize::max));
		}
		width = width.to_string().len();
		
		for row in self.image.as_slice() {
			let mut line = String::from("[");
			let columns = row.as_slice();
			for column in &columns[..columns.len() - 1] {
				line += format!("{:01$}, ", column, width).as_str();
			}
			line += format!("{:01$}", columns[columns.len() - 1], width).as_str();
			let _ = writeln!(f, "{}]", line);
		}
		Ok(())
	}
}

impl Index<usize> for Image {
	type Output = Vec<usize>;
	
	fn index(&self, index: usize) -> &Self::Output {
		&self.image[index]
	}
}

impl IndexMut<usize> for Image {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		&mut self.image[index]
	}
}

impl Image {
	fn make_rect(width: usize, length: usize) -> Self {
		let mut outer = Vec::new();
		for row in 0..length {
			let mut inner = Vec::new();
			for column in 0..width {
				inner.push(row * width + column + 1);
			}
			outer.push(inner);
		}
		Self {
			image: outer,
		}
	}
	
	fn make_square(width: usize) -> Self {
		Self::make_rect(width, width)
	}
	
	fn width(&self) -> usize {
		self.image[0].len()
	}
	
	fn length(&self) -> usize {
		self.image.len()
	}
	
	fn transpose(&self) -> Self {
		let mut outer = Vec::new();
		for row in 0..self.width() {
			let mut inner = Vec::new();
			for column in 0..self.length() {
				inner.push(self.image[column][row]);
			}
			outer.push(inner);
		}
		Self {
			image: outer,
		}
	}
	
	fn rotate_clockwise(&self) -> Self {
		let width = self.width();
		let length = self.length();
		let mut new_image = Self::make_rect(width, length);
		
		for row in 0..width {
			for column in 0..length {
				new_image[column][width - 1 - row] = self.image[row][column];
			}
		}
		new_image
	}
	
	fn rotate_counterclockwise(&self) -> Self {
		let width = self.width();
		let length = self.length();
		let mut new_image = Self::make_rect(width, length);
		
		for row in 0..width {
			for column in 0..length {
				new_image[length - 1 - column][row] = self.image[row][column];
			}
		}
		new_image
	}
	
	fn hflip(&self) -> Self {
		let mut outer = Vec::new();
		
		for row in self.image.iter() {
			outer.push(row.iter().cloned().rev().collect::<Vec<usize>>());
		}
		Self {
			image: outer,
		}
	}
	
	fn vflip(&self) -> Self {
		Self {
			image: self.image.iter().cloned().rev().collect(),
		}
	}
}

fn main() {
	let mut args: Vec<String> = env::args().collect();
	if args.len() == 1 {
		args.push(String::from("7"));
	}
	let image_size: usize = (&args[1]).parse().unwrap();
	
	let image = Image::make_square(image_size);
	println!("Original:\n{}", image);
	
	let image_length = 1.max(image_size / 2) + 1.max(image_size / 5) + 3;
	let image2 = Image::make_rect(image_size, image_length);
	println!("\nRectangle:\n{}", image2);
	
	println!("\nTransposed:\n{}", image.transpose());
	println!("\nRotate Right:\n{}", image.rotate_clockwise());
	println!("\nRotate Left:\n{}", image.rotate_counterclockwise());
	println!("\nHorizontal Flip:\n{}", image.hflip());
	println!("\nVertical Flip:\n{}", image.vflip());
	println!();
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
