#!/usr/bin/python3

'''An exploration into simple numeric 'image' manipulation.'''

from doctest import testmod
from sys import argv, exit as sys_exit
from typing import Sequence

Image = Sequence[Sequence[int]]

def parse_int(inputValue: str, defaultValue: int | None = None, /) -> int | None:
	'''
	Try to parse a str as an int and return a default on failure.
	
	>>> parse_int('1')
	1
	>>> parse_int('s') == None
	True
	>>> parse_int('t', 3)
	3
	'''
	
	try:
		return int(inputValue)
	
	except ValueError:
		return defaultValue

def print_image(image: Image, /) -> None:
	'''Cleanly print an Image.'''
	
	width = len(str(max([max(line) for line in image])))
	for row in image:
		line = '['
		for column in row:
			line += f"{column:0{width}}, ".format(width=width)
		print(line[:-2] + ']')

def make_rectangular_image(width: int, length: int, /) -> Image:
	'''Make an rectangular Image.'''
	
	assert width > 0 and length > 0, 'Image must have a positive area.'
	
	return [[row * width + column + 1 for column in range(width)] for row in range(length)]

def make_square_image(sideLength: int, /) -> Image:
	'''Make a square Image of width sideLength.'''
	
	return make_rectangular_image(sideLength, sideLength)

def transpose_image(image: Image, /) -> Image:
	'''Return the transpose of an Image.'''
	
	return [[image[column][row] for column in range(len(image))] for row in range(len(image[0]))]

def rotate_image_clockwise(image: Image, /) -> Image:
	'''Rotate an Image clockwise.'''
	
	# return [row[::-1] for row in transpose_image(image)]
	
	(width, length) = (len(image[0]), len(image))
	new_image: Image = [[-1 for _ in range(length)] for _ in range(width)]
	
	for row in range(width):
		for column in range(length):
			new_image[column][width - 1 - row] = image[row][column] #type:ignore
	return new_image

def rotate_image_counterclockwise(image: Image, /) -> Image:
	'''Rotate an Image counter-clockwise.'''
	
	#return transpose_image(image)[::-1]
	
	(width, length) = (len(image[0]), len(image))
	new_image: Image = [[-1 for _ in range(length)] for _ in range(width)]
	
	for row in range(width):
		for column in range(length):
			(new_image[length - 1 - column])[row] = image[row][column] #type:ignore
	return new_image

def main(arguments: Sequence[str], /) -> int:
	'''Main function.'''
	
	image_size = parse_int(arguments[1] if len(arguments) > 1 else '', 7)
	assert image_size is not None
	image = make_square_image(image_size)
	
	print('Original:')
	print_image(image)
	
	print('\nRectangle:')
	image_length = max(1, (image_size // 2)) + max(1, (image_size // 5)) + 3
	image2 = make_rectangular_image(image_size, image_length)
	print_image(image2)
	
	print('\nTransposed:')
	print_image(transpose_image(image))
	
	print('\nRotate Right:')
	print_image(rotate_image_clockwise(image))
	
	print('\nRotate Left:')
	print_image(rotate_image_counterclockwise(image))
	
	print('\nHorizontal Flip:')
	print_image([row[::-1] for row in image])
	
	print('\nVertical Flip:')
	print_image(image[::-1])
	
	print('')
	return 0

if __name__ == '__main__':
	CODE = 0
	if len(argv) > 1 and argv[1] in ('-t', '--test'):
		testmod()
	else:
		CODE = main(argv)
	sys_exit(CODE)

# vim: set ts=4 sw=0 sts=-1 noet ai sta:
