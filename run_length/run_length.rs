// Run length encoding.

use std::fmt::Write;

fn encode_runs(raw_input: &str) -> String {
	let mut result = String::new();
	
	if raw_input.is_empty() {
		return result
	}
	
	let mut counter = 1;
	let characters: Vec<char> = raw_input.chars().collect();
	let mut prev_char = characters[0];

	for &character in &characters[1..] {
		if character == prev_char {
			counter += 1;
		}
		else {
			let _ = write!(result, "{}{}", counter, prev_char);
			counter = 1;
			prev_char = character;
		}
	}
	
	format!("{}{}{}", result, counter, prev_char)
}

fn decode_runs(raw_input: &str) -> String {
	let mut result = String::new();
	
	if raw_input.is_empty() || raw_input.len() % 2 == 1 {
		return result
	}
	
	let mut runs: Vec<(usize, char)> = Vec::new();
	let characters: Vec<char> = raw_input.chars().collect();
	for index in (0..raw_input.len()).step_by(2) {
		let val: usize = characters[index].to_string().parse().unwrap();
		runs.push((val, characters[index + 1]));
	}
	
	for (length, character) in runs.iter() {
		for _ in 0..*length {
			result.push(*character);
		}
	}
	
	result
}

fn main() {
	assert_eq!(encode_runs(""), "");
	assert_eq!(encode_runs("a"), "1a");
	assert_eq!(encode_runs("abcd"), "1a1b1c1d");
	assert_eq!(encode_runs("aabcc"), "2a1b2c");
	assert_eq!(encode_runs("aaabccdaab"), "3a1b2c1d2a1b");
	assert_eq!(decode_runs(""), "");
	assert_eq!(decode_runs("3a1"), "");
	assert_eq!(decode_runs("1a"), "a");
	assert_eq!(decode_runs("1a1b1c1d"), "abcd");
	assert_eq!(decode_runs("2a1b2c"), "aabcc");
	assert_eq!(decode_runs("3a1b2c1d2a1b"), "aaabccdaab");
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
