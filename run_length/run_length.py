#!/usr/bin/python3

'''Run length encoding.'''

from doctest import testmod

def encode_runs(raw_input: str) -> str:
	'''
	>>> encode_runs('')
	''
	>>> encode_runs('a')
	'1a'
	>>> encode_runs('abcd')
	'1a1b1c1d'
	>>> encode_runs('aabcc')
	'2a1b2c'
	>>> encode_runs('aaabccdaab')
	'3a1b2c1d2a1b'
	'''
	
	if len(raw_input) == 0:
		return ''
	
	(counter, prev_char) = (1, raw_input[0])
	result = ''

	for char in raw_input[1:]:
		if char == prev_char:
			counter += 1
		else:
			result += f"{counter}{prev_char}"
			(counter, prev_char) = (1, char)
	
	return f"{result}{counter}{prev_char}"

def decode_runs(raw_input: str) -> str:
	'''
	>>> decode_runs('')
	''
	>>> decode_runs('3a1')
	''
	>>> decode_runs('1a')
	'a'
	>>> decode_runs('1a1b1c1d')
	'abcd'
	>>> decode_runs('2a1b2c')
	'aabcc'
	>>> decode_runs('3a1b2c1d2a1b')
	'aaabccdaab'
	'''
	
	if len(raw_input) == 0 or len(raw_input) % 2 == 1:
		return ''
	
	runs = [(int(raw_input[index]), raw_input[index+1]) for index in range(0, len(raw_input), 2)]
	result = ''
	
	for (length, char) in runs:
		result += char * length
	
	return result

if __name__ == '__main__':
	testmod()
