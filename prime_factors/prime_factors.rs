// An exploration into prime numbers.

use std::collections::BTreeMap;
use std::env;

fn make_primes(limit: u64) -> Vec<u64> {
	// Generate prime numbers <= sqrt(limit).
	
	let mut primes = Vec::new();
	
	if limit < 4 {
		return primes
	}
	
	primes.push(2);
	
	if limit < 9 {
		return primes
	}
	
	primes.push(3);
	
	// a candidate number c must be such that 2 <= c <= sqrt(n)
	let high_val = (limit as f64).sqrt().ceil() as u64;
	for candidate in (5..=high_val).step_by(2) {
		// if n % 6 in (0, 2, 4): n is divisible by 2 and thus not prime
		// if n % 6 in (0, 3): n is divisible by 3 and thus not prime
		// both of the above are accounted for by putting 2 and 3 before this loop
		let tmp = candidate % 6;
		if tmp == 1 || tmp == 5 {
			primes.push(candidate);
		}
	}
	primes
}

fn format_factors(value: u64, factors: &BTreeMap<u64, u64>) -> String {
	// Format a number and its prime factors in expression form.
	
	let mut output = format!("    {value} ");
	if factors.len() == 1 && factors.get(&value).is_some() {
		output.push_str("is prime");
		return output
	}
	else if factors.is_empty() { //1
		output.push_str("is NOT prime");
		return output
	}
	
	output.push_str("= ");
	for (prime, count) in factors.iter() {
		let mut exponent = String::from("");
		if count > &1 {
			exponent = format!("^{count}");
		}
		output.push_str(format!("{prime}{exponent} * ").as_str());
	}
	output[..output.len() - 3].to_string()
}

fn main() {
	let mut args: Vec<String> = env::args().collect();
	if args.len() == 1 {
		// an interesting composite number default
		args.push(String::from("510510"));
	}
	
	let input_value: u64 = args[1].parse().unwrap();
	let mut working_value = input_value;
	let mut factors = BTreeMap::new();
	
	for prime in make_primes(working_value) {
		if working_value == 1 {
			break
		}
		while working_value % prime == 0 {
			let mut val = 0;
			if let Some(&count) = factors.get(&prime) {
				val = count;
			}
			factors.insert(prime, val + 1);
			working_value /= prime;
		}
	}
	
	// the input itself is prime
	if working_value > 1 {
		factors.insert(working_value, 1);
	}
	
	println!("{}", format_factors(input_value, &factors));
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
