#!/usr/bin/python3

'''A simple program to encode and decode a string in base64.'''

from argparse import ArgumentParser
from doctest import testmod
from sys import argv, exit as sys_exit
from typing import Generator, Sequence

def char_range(char1: str, char2: str, /) -> Generator[str, None, None]:
	'''Create a generator for a range of character values.'''
	
	assert len(char1) == 1 and len(char2) == 1, 'Ranges can only be made from characters.'
	
	direction = ((char1 > char2) - (char1 < char2)) * -1
	for char in range(ord(char1), ord(char2) + direction, direction):
		yield chr(char)

B64_CHARS = [*char_range('A', 'Z')] \
		  + [*char_range('a', 'z')] \
		  + [*char_range('0', '9')] \
		  + ['+', '/', '=']

def my_encode(input_value: str, /) -> str:
	'''
	My implementation of the b64 encode function.
	
	>>> my_encode('message')
	'bWVzc2FnZQ=='
	'''
	
	text_chars = ''.join([format(ord(c), '08b') for c in input_value])
	orig_len = len(text_chars)
	# input must be padded to a multiple of 24 bits.
	padded_text = text_chars + '0' * (24 - (orig_len % 24))
	
	# the -1 leads to the padding character.
	char_vals = [padded_text[i:i+6] if i < orig_len else '-1' for i in range(0, len(padded_text), 6)]
	return ''.join([B64_CHARS[i] for i in map(lambda val: int(val, base=2), char_vals)])

def my_decode(input_value: str, /) -> str:
	'''
	My implementation of the b64 decode function.
	
	>>> my_decode('bWVzc2FnZQ==')
	'message'
	'''
	
	indices = map(B64_CHARS.index, [*input_value])
	binary = ''.join([format(0 if i == len(B64_CHARS) - 1 else i, '06b') for i in [*indices]])
	char_vals = [*map(lambda val: int(val, base=2), [binary[i:i+8] for i in range(0, len(binary), 8)])]
	while 0 in char_vals:
		char_vals.remove(0) # Remove the padding character(s).
	return ''.join([*map(chr, char_vals)])

def make_parser() -> ArgumentParser:
	'''Make a parser for program options.'''
	
	from argparse import RawTextHelpFormatter as RTHF
	
	parser = ArgumentParser(description=globals()['__doc__'], formatter_class=RTHF, allow_abbrev=False)
	
	parser.add_argument('action', choices=['encode', 'decode'], help='what to do with the input')
	parser.add_argument('input', help='the value to be converted')
	
	system_help = 'whether or not to use the system library'
	parser.add_argument('-s', '--system', action='store_true', help=system_help, dest='use_system')
	
	return parser

def main(arguments: Sequence[str], /) -> int:
	'''Main function.'''
	
	from base64 import standard_b64decode, standard_b64encode
	
	args = make_parser().parse_args(arguments)
	# args = parser.parse_args(['encode', 'message'])
	# args = parser.parse_args(['decode', 'bWVzc2FnZQ=='])
	
	functions = {
		'encode': {
			True: lambda text: standard_b64encode(text.encode()).decode(),
			False: my_encode,
		},
		'decode': {
			True: lambda text: standard_b64decode(text.encode()).decode(),
			False: my_decode,
		},
	}
	
	print(functions[args.action][args.use_system](args.input))
	return 0

if __name__ == '__main__':
	CODE = 0
	if len(argv) > 1 and argv[1] in ('-t', '--test'):
		testmod()
	else:
		CODE = main(argv[1:])
	sys_exit(CODE)

# vim: set ts=4 sw=0 sts=-1 noet ai sta:
